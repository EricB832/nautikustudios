﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NautikuStudios.Controllers {
    public class HomeController : Controller {
        public ActionResult Index() {

            return View();
        }

        public ActionResult Shoppe() {

            return View();
        }

        public ActionResult About() {

            return View();
        }

        public ActionResult Contact() {

            return View();
        }

        public ActionResult Donate() {

            return View();
        }

        
    }
}
